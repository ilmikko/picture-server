#/bin/env ruby
#
# A simple picture server.
#

Dir.chdir(__dir__);
$LOAD_PATH.push('lib');
Thread.abort_on_exception = true;

require('bundler/setup');

def scan(folder)
	filenames = 'png,jpg,jpeg';
	Dir["#{folder}/*.{#{filenames}}"] + Dir["#{folder}/*/"].map{ |folder| scan folder; }.flatten;
end

$SCAN_FOLDER = './examples/';
$HOST = 'localhost';
$PORT = '2345';

$images = scan $SCAN_FOLDER;

puts("Found #{$images.length} images to serve.");

require('server');

Server.new($HOST,$PORT);
