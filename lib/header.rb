class Header
	def [](k)
		@headers[k.to_s.downcase];
	end
	def []=(k,v)
		@headers[k.to_s.downcase]=v.to_s;
	end

	def to_s
		@headers.map{|k,v| k+': '+v }.join("\r\n");
	end

	def inspect
		to_s;
	end

	#######
	private
	#######

	def initialize(**args)
		@headers={};
		args.each{|k,v|
			self[k]=v;
		}
	end
end
