require('socket');
require('timeout');
require('header');

class Server
	@@mimes={
		'png' => 'image/png',
		'jpg' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'webp' => 'image/webp',
		'gif' => 'image/gif',
	};

	def respond(status="200 OK",headers={},response)
	end

	def mime(path)
		@@mimes[File.extname(path).split(".").last]||'image/png';
	end

	def initialize(host, port, timeout:20)
		@server = TCPServer.new(host,port);

		puts("Server listening on #{host} on port #{port}");

		loop{
			socket = @server.accept;

			Thread.new{
				request = socket.gets;
				puts request;

				resp_headers = Header.new(connection: 'close');

				response = "\r\n";
				begin
					Timeout::timeout(timeout){
						# Read and parse request headers
						headers={};
						while true
							header_raw = socket.gets;
							break if header_raw == "\r\n" or header_raw == "\n";
							header_raw = header_raw.split(':');
							headers[header_raw.shift.strip.downcase] = header_raw.join(':').strip;
						end

						# Read the file
						path = $images.sample;

						File.open(path,'rb'){|file|
							# Status
							socket.print("HTTP/1.1 200 OK\r\n");

							resp_headers['content-type'] = mime(path);
							resp_headers['content-length'] = file.size;

							# Headers
							socket.print("#{resp_headers.to_s}\r\n");

							# Split
							socket.print("\r\n");
							IO.copy_stream(file, socket);
						};

						socket.close;
					}
				rescue Timeout::Error => e
					response = "\r\n";
					# Status
					socket.print("HTTP/1.1 408 Request Timeout\r\n");

					# Headers
					socket.print("#{headers.to_s}\r\n");

					# Split
					socket.print("\r\n");

					# Body
					socket.print(response);

					socket.close;
				end
			}
		}
	end
end
